const menuList = [
  {
    name: '大盘',
    key: 'all',
    children: [
      // {
      //   name: '整体',
      //   key: 'all',
      // },
      {
        name: '注册',
        key: 'register',
      },
      {
        name: '实名',
        key: 'auth',
      },
      {
        name: '提现',
        key: 'withdraw',
      },
      {
        name: '充值',
        key: 'deposits',
      },
      {
        name: '每日余额',
        key: 'daily_funds'
      },
    ],
  },
];

export {
  menuList,
};
