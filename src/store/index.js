import Vue from 'vue';
import Vuex from 'vuex';


Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
  },
  mutations: {
    updateState: (state, { payload }) => {
      Object.keys(payload).forEach((r) => {
        state[r] = payload[r];
      });
    },
  },
  actions: {
  },
});

export default store;

