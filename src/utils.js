function formatter(date, format = 'yyyy/MM/dd') {
  let fmt = '';
  const newDate = new Date(date);
  const o = {
    'M+': newDate.getMonth() + 1,
    'd+': newDate.getDate(),
    'h+': newDate.getHours(),
    'm+': newDate.getMinutes(),
    's+': newDate.getSeconds(),
  };
  if (/(y+)/.test(format)) fmt = format.replace(RegExp.$1, (newDate.getFullYear() + '').substr(4 - RegExp.$1.length));
  for (let k in o) {
    if (new RegExp(`(${k})`).test(format)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? o[k] : ((`00${o[k]}`).substr((`${o[k]}`).length)));
  }
  return fmt;
};

export {
  formatter,
};