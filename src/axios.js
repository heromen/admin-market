import axios from 'axios';
import { Message } from 'iview';


axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.timeout = 5000;

axios.interceptors.response.use((res) => {
  if (!res.data.success) {
    Message.error(res.data.errors.message);
    return Promise.reject(res.data);
  }
  return res.data;
}, (err) => {
  Message.error(err);
  return Promise.reject(err);
});


export function fetch(url, params, header) {
  const token = localStorage.getItem('MARKET_TOKEN');
  if (header !== 'false') {
    axios.defaults.headers.Authorization = `jwt ${token}`;
  }
  return new Promise((res, rej) => {
    axios.post(url, params)
      .then((response) => {
        res(response);
      }, (err) => {
        if (err.request.status === 401) {
          location.href = '#/login';
        }
        rej(err);
      })
      .catch(err => rej(err));
  });
}

export default {
  // AdminMarkets(params) {
  //   return fetch('/admin/analysis/market', params);
  // },
  AdminMarkets(params) {
    return fetch('/admin/analysis/markets/total.json', params);
  },
  Login(params) {
    return fetch('/admin/session.json', params, 'false');
  },
  SendCode(params) {
    return fetch('/admin/session/send_sms.json', params, 'false');
  },
  queryMarkets(params) {
    return fetch('/admin/analysis/market.json', params);
  },
};
