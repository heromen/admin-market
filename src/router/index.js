import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const mainRouter = {
  path: '/home',
  name: 'main',
  redirect: '/home/register',
  component: () => import('@/components/main'),
  children: [
    { path: 'all', title: 'home', name: 'all', component: () => import('@/components/home'), },
    { path: 'register', title: '注册', name: 'register', component: () => import('@/components/home/register'), },
    { path: 'auth', title: '实名', name: 'auth', component: () => import('@/components/home/auth'), },
    { path: 'withdraw', title: '提现', name: 'withdraw', component: () => import('@/components/home/withdraw'), },
    { path: 'deposit', title: '充值', name: 'deposits', component: () => import('@/components/home/deposit'), },
    { path: 'dailyFunds', title: '每日余额', name: 'daily_funds', component: () => import('@/components/home/dailyFunds'), },
  ],
};

const loginRouter = [{
  path: '/',
  name: 'root',
  redirect: '/login',
}, {
  path: '/login',
  name: 'login',
  component: () => import('@/components/login'),
}, {
  path: '*',
  name: '404',
  component: () => import('@/components/404'),
}];

const routes = {
  routes: [...loginRouter, mainRouter],
};
const router = new Router(routes);

export default router;

// routers.beforeEach((to, from, next) => {
//   // 这里加逻辑
//   next();
// });

// routers.afterEach(() => {
//   window.scrollTo(0, 0);
// });

